import React from "react"
import { Link } from "gatsby"

import sun from '../../content/assets/sun-icon.png'
import leftArrow from '../../content/assets/left-arrow.png'
import gitlab from '../../content/assets/gitlab.png'
import linkedin from '../../content/assets/linkedin.png'
import kofi from '../../content/assets/kofi.png'

const Layout = ({ location, title, children }) => {
  const rootPath = `${__PATH_PREFIX__}/`
  const isRootPath = location.pathname === rootPath
  let header

  if (isRootPath) {
    header = (
      <h1 className="main-heading">
        <span>
          <img src={sun} alt="logo" />
        </span>{' '}
        {title}
      </h1>
    )
  } else {
    header = (
      <Link className="header-link-home" to="/">
        <span>
          <img src={leftArrow} alt="Left Arrow" />
        </span>{' '}
        {title}
      </Link>
    )
  }

  return (
    <div className="global-wrapper" data-is-root-path={isRootPath}>
      <header className="global-header">{header}</header>
      <main>{children}</main>
      <footer className="footer flex">
        <section className="container">
          <nav className="flex">
            <a
              href="https://gitlab.com/lucianciobanu"
              title="Open Source on GitLab"
              target="_blank"
              rel="noopener noreferrer"
              className="img"
            >
              <img src={gitlab} className="footer-img" alt="LC's Gitlab" />
            </a>
            <a
              href="https://linkedin.com/in/lucianciob4nu"
              title="Work Experience"
              target="_blank"
              rel="noopener noreferrer"
              className="img"
            >
              <img src={linkedin} className="footer-img" alt="LC's LinkedIn" />
            </a>
            <a
              href="https://ko-fi.com/lucianciobanu"
              title="Treat me with a coffee"
              target="_blank"
              rel="noopener noreferrer"
              className="img"
            >
              <img src={kofi} className="footer-img" alt="LC's Ko-Fi" />
            </a>
          </nav>
        </section>
      </footer>
    </div>
  )
}

export default Layout
