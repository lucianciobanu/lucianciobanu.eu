---
layout: blog
title: mat-checkbox change event example
category: Web Dev
language: EN
date: 2021-05-28T09:19:18.021Z
---
I'm putting this down to remember the time I searched too long for the material checkbox change event so here is an example of triggering a function on change:

```html
<div *ngFor="let item of data; let i = index;">
  <mat-checkbox [checked]="item.checked" (change)="onChange($event, i, item)">
    {{item.label}}
  </mat-checkbox>
</div>
```

```javascript
onChange(event, index, item) {
  item.checked = !item.checked;
  console.log(index, event, item);
}
```

Hope this helped

Cheers,\
Lucian