---
layout: blog
title: Angular View Encapsulation to Style Material Stepper
category: Web Dev
language: EN
technology:
  - Angular
  - AngularMaterial
date: 2022-03-01T10:10:43.601Z
---
I find Angular's Material Stepper as very handy to use while wanting some more screen real estate. I often use it when I trigger a pop up in which the user needs to perform more actions than the pop up can offer and I've spent some time recently to find out how I can better control the navigation through it. Basically, most of the time I want to navigate between steps via buttons and want to take out the material stepper navigation on top so that the user can't jump between steps and has to follow my flow. In order to do this, I had to apply some styling to the stepper but the pre existing material stepper styling had to be overwritten and as ng-deep is deprecated, I spent some time learning about view encapsulation so here it is:

Step 1: make the stepper linear. assign it a variable if you want to use the stepper's properties of next and previous from the class (I preffer to use buttons with matStepperNext and matStepperPrevious attributes when I can), and give it a very specific class for styling, as such:

```html
<mat-horizontal-stepper #stepper linear class="example-modal-stepper">
  <mat-step>
    <button matStepperNext mat-flat-button></button>
  </mat-step>
  <mat-step>
    <button matStepperPrevious mat-flat-button></button>
  </mat-step>
</mat-horizontal-stepper>
```

Step 2: use the css file to remove the stepper's navigation; I did it as such:

```sass
mat-horizontal-stepper {
  &.example-modal-stepper {
    .mat-horizontal-stepper-header-container {
      display: none;
    }
    
    .mat-step-header {
      pointer-events: none;
    }
  }
}
```

I am using the "example-modal-stepper" class to increase specificity but also to make sure that this styling, will only apply here, although I will make it global for it to overwrite the standard material design styling.

Step 3: make the styling global with view encapsulation from the component's .ts file

```typescript
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: [...],
  templateUrl: [...],
  styleUrls: [...],
  encapsulation: ViewEncapsulation.None
})
```

So, what I'm doing here, by chosing the None property, I'm making all scss code directly available in "style" so it will overwritte the standard material design styling.

Hope this helped!

Cheers,\
Lucian