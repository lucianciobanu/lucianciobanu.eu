---
layout: blog
title: Angular Material Dialog Close Unit Test
category: Web Dev
language: EN
technology:
  - JavaScript
  - Angular
  - UnitTest
date: 2021-04-07T11:08:10.014Z
---
Another day, another unit testing challenge. I swear this feels more difficult than implementing the functionality. Right... The modal component implemented with material dialog has the following piece of code which needs to be tested:

```javascript
[...]
 
 closeModal() {
   this.dialogRef.close();
 }
```

In order to do this, I had to implement the following in the modal's component.spec.ts file:

```javascript
import { MatDialogRef, MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';

[...]
 
 beforeEach(async () => {
   await TestBed.configureTestingModule({
     declarations: [ MyModalComponent ],
     imports:[ MatDialogModule ],
     providers: [
       { provide: MAT_DIALOG_DATA, useValue: {} },
       { provide: MatDialogRef, useValue: { close: () => {} } }
     ]
   })
   .compileComponents();
 });

[...]
 
 it('should close dialog on trigger', () => {
   spyOn(diaolgRef, 'close');
   component.closeModal();
   expect(dialogRef.close).toHaveBeenCalled();
 });
```

Hope this helped!

Cheers,\
Lucian!