---
layout: blog
title: " ng build --base-href flag not setting correct relative file path"
category: Web Dev
language: EN
technology:
  - Angular
date: 2021-05-25T08:49:45.342Z
---
The customer I'm currently working with on a Angular solution is hosting a couple of webapps on the same server for dev environment and the dev ops flow is made of pulling latest dev branch, running a build and copy & pasting the resulting dist contents to an IIS and the person responsible of this wanted to automate the inclusion of /extra/ in the "index.js" href but the recommended Angular documentation way of doing this - adding a flag in ng build as:

`ng build --base-href /extra/`

does not work; for some reason, angular-cli will add just "extra/" to the href in index.js

the best solution I could think of is to add in "package.json" under "scripts" the same flag and then just call the new script; This works very well for my current situation.

`"build-href-extra": "ng build --base-href /extra/"`

So now when calling ng build-href-extra, the flag is being used properly by the build.

Hope this helped!

Cheers,\
Lucian