---
layout: blog
title: Netlify CMS Gatsby.js reset password bad link
category: Web Dev
language: EN
technology:
  - NetlifyCMS
  - Gatsby.js
date: 2021-01-25T08:47:19.430Z
---
I'm a really big fan of Netlify and it's ease of deployment. However, when I started this blog on Gatsby.js with Netlify CMS, I was expecting things to be a bit more plug and play. An annoyance I came across was that I had forgotten my admin password and after clicking on "Reset password" I'd get a link in my inbox but the link would open **www.lucianciobanu.eu/recovery-token**\
Luckily I've been gifted a bit of flair so I realised I need to add /admin before the /recovery-token (www.lucianciobanu.eu/admin/recovery-token) and this took me to the page which allowed me to update my password. 

But in order to avoid having to rely on flair or my password forgetting memory again, there is a much cleaner way to have "admin" added already to the linkes in the emails the users receive:

1. in my static/admin folder, I have created four new HTMLs (confirmation, email-change, invitation and recovery) files as such:

![file structure](/images/uploads/file-structure.png "Gatsby starter blog with email templates")

2. Add the following html in the four files:

confirmation.html

```html
<h2>Confirm your signup</h2>

<p>Follow this link to confirm your user:</p>
<p><a href="{{ .SiteURL }}/admin/#confirmation_token={{ .Token }}">Confirm your mail</a></p>
```

recovery.html

```html
<h2>Reset Password</h2>

<p>Follow this link to reset the password for your user:</p>
<p><a href="{{ .SiteURL }}/admin/#recovery_token={{ .Token }}">Reset Password</a></p>
```

invitation.html

```html
<h2>You have been invited</h2>

<p>You have been invited to create a user on {{ .SiteURL }}. Follow this link to accept the invite:</p>
<p><a href="{{ .SiteURL }}/admin/#invite_token={{ .Token }}">Accept the invite</a></p>
```

email-change.html

```html
<h2>Confirm Change of Email</h2>

<p>Follow this link to confirm the update of your email from {{ .Email }} to {{ .NewEmail }}:</p>
<p><a href="{{ .SiteURL }}/admin/#email_change_token={{ .Token }}">Change Email</a></p>
```

At this point you can commit your changes and push them to the production branch because the last step is only about updating some settings in your netlify account.

3. got to netlify to your site, settings and under identity, emails, update your templates paths as such:

![site settings identity emails](/images/uploads/site-settings-identtiy.png "Site settings identity emails")

Hope this helped!

Cheers,\
Lucian