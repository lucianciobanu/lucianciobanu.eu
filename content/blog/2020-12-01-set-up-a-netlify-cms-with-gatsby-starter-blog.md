---
layout: blog
title: Set up a Netlify CMS with Gatsby starter blog
category: Web Dev
language: EN
technology:
  - gatsby
  - netlify
date: 2020-12-01T15:58:01.774Z
---
**Gatsby.js** promises to offer a quick way to build websites and blogs and together with deploying and hosting by **Netlify**, it has to be one of the fastest ways I came across so far to build an online presence. As I've promised my self a long time ago I'd start a blog, this was the perfect challenge for me to bring it all together and live the dream. What I was unsure about the most was how I could create a pleasant CMS to use for my future blog but Netlify offers that too and I'm going to show you how I did it.

[](https://www.netlifycms.org/)Netlify CMS is a Content Management System for static site generators so it works great with Gatsby.js. It allows you to create and edit content as if it was WordPress, but it's a much simpler and user-friendly interface.

After generating your Gatsby site; I started with:

`gatsby new my-blog-name https://github.com/gatsbyjs/gatsby-starter-blog`

(the above line works after you've installed npm on your machine and gatsby via npm)

There are 4 steps to take:

1. add files to the Gatsby.js structure
2. configuration
3. authentication
4. access the CMS

So here we go:

###### 1. Adding the files

In the `static` folder, which should be at the same level wit `src`, create two files: `index.html` and `config.yaml`Index.html is the entry point to your CMS so you will need to add the following code inside it:

```html
<!doctype html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Content Manager</title>
</head>
<body>
  <script src="https://unpkg.com/netlify-cms@^2.0.0/dist/netlify-cms.js"></script>
</body>
</html>
```

###### 2. Configuration

In config.yaml, you tell Netlify where to post your new articles, where to store the images you might want to include and also what fields you wish your editor to have. The fields are called widgets and there are a lot of options - you can have selects, lists, datetime, strings, numbers and markdown, the actual place you write your blog and after some thought, this is the configuration I decided upon but you should mix it according to your needs:

```yaml
backend:
  name: git-gateway
  branch: production

media_folder: "images/uploads"

collections:
  - name: "blog"
    label: "LC.eu posts"
    folder: "content/blog"
    create: true
    slug: "{{year}}-{{month}}-{{day}}-{{slug}}"
    fields:
      - {label: "Layout", name: "layout", widget: "hidden", default: "blog"}
      - {label: "Title", name: "title", widget: "string"}
      - {label: "Category", name: "category", widget: "select", options: ["Web Dev", "Interior Design", "Car Care"]}
      - {label: "Language", name: "language", widget: "select", options: ["EN", "RO"]}
      - {label: "Technology", name: "technology", widget: "list", hint: "Press comma for new technology to be inserted"}
      - {label: "Publish Date", name: "date", widget: "datetime"}
      - {label: "Body", name: "body", widget: "markdown"}
```

I want Netlify CMS to post to production branch, store images in the images/uploads folders which also need to be created under `static/admin` so at the same level with the above mentioned index.html and config.yaml.

I've also added a "Category" select, a "Language" select and a "Technology" list alongside the usual "Publish Date" datetime and "Body" markdown. There are more widgets available and you can even create your own.

This was the first time I was working with a yaml file so I found that [yamllint](http://www.yamllint.com) is a useful resource as it tells you wether your defined collections makes sense or not.

All Netlify CMS standard widgets can be found [here.](https://www.netlifycms.org/docs/widgets/)

###### 3. Authentication

All I had to do was add `<script src="https://identity.netlify.com/v1/netlify-identity-widget.js"></script>` to the admin/index.html file, right between the `<head>` tags but also in public/index.html. In public/index.html, you will also have to add the following code before the closing body tag:

```javascript
<script>
  if (window.netlifyIdentity) {
    window.netlifyIdentity.on("init", user => {
      if (!user) {
        window.netlifyIdentity.on("login", () => {
          document.location.href = "/admin/";
        });
      }
    });
  }
</script>
```

This was a bit annoying because the code comes formatted in the form of a single line but that's the hardest thing about it.

After deploying your site with netlify, chose your site, go to "Site Settings", and from the left panel, chose "Identity" and then click on "Enable Identity"

After that, chose "Registration" from under "Identity" from the left panel and for the moment select "open". I see Netlify offers the option of "invite only" but I couldnt make the link I was receiving in my email invite work so I found this work around - first leave it open, quickly create an account and then set it back to invite only so nobody else can create an account.

You'll also need to go to "Services" and click on "Enable Git Gateway" so Netlify CMS will be able to make a commit and push to the production branch you've set in the YAML file. And if this branch is the same as the one selected in Netlify for deploy, the new blog post will be visible online ASAP. But after enabling the Git Gateway, you should be good to go, all that's left is to got to https://yoursite.com/admin, chose a username and password if you have left "Open" under "Registration" form the "Identity" left panel and after logging in, make sure to chage this setting back to "Invite only" so you dont have anyone just creating a user account for themselves. It's not a great workaround but those email invites are not up to speed yet; at least not for me.

Anyway, by clicking on "New", you'll be ready to write your first post. Also, dont forget to delete the initial posts that came with the blog starter from github when creating the gatsby project.

Hope this helped!

Cheers,\
Lucian

P.S.: special thanks to Cristi Todoran for being the absolute legend and inspiration he is!