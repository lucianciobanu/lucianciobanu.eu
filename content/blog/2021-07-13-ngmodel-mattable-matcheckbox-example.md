---
layout: blog
title: NgModel MatTable MatCheckBox example
category: Web Dev
language: EN
technology:
  - Angular
  - Material
date: 2021-07-13T13:59:21.845Z
---
I'm enjoying using Angular's bananabox magic more and more and have just found another use case for NgModel.

For this specific case, I had to display data and just alow the user to modify a property between true and false so I though of using material table, material checkbox and was pleasantly surprised to see that \[(ngModel)] can also be linked to the table's dataSource.data array.

```typescript
<mat-table [dataSource]="dataSource">
  <ng-container matColumnDef="Inactive">
    <mat-header-cell class="d-flex justify-content-center" *matHeaderCellDef>Inactive</mat-header-cell>
    <mat-cell class="d-flex justify-content-center" *matCellDef="let outage; let i = index">
      <mat-checkbox [(ngModel)]="dataSource.data[i].Inactive"></mat-checkbox>
    </mat-cell>
  </ng-container>
</mat-table>
```

And that's it, really. Now the dataSource will update on checkbox change.

Hope this helped!

Cheers,\
Lucian