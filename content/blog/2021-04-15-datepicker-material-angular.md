---
layout: blog
title: Datepicker Material Angular
category: Web Dev
language: EN
technology:
  - HTML
  - JavaScript
  - Angular
  - AngularMaterial
date: 2021-04-15T11:21:00.856Z
---
Today I had to implement a Material Angular Datepicker and this is how I did it:

home.component.html

```html
<form class="form" [formGroup]="form">
  <mat-form-field appearance="outline" [floatLabel]="'always'">
    <mat-label>Date</mat-label>
    <input matInput formControlName="date" [matDatepicker]="picker" placeholder="Select Date" />
    <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
    <mat-datepicker #picker></mat-datepicker>
  </mat-form-field>
</form>
```

home.component.ts

```javascript
import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  
  public form: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.formInit();
  }
  
  private formInit() {
    this.form = this.formBuilder.group({
      date: ''
    });
  }

}
  
```

app.module.ts

```javascript
[...]
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

[...]
 
 //also add above to imports array
```

Hope this helped!

Cheers,\
Lucian