---
layout: blog
title: Algoritmic for new line in Tooltip formatting
category: Web Dev
language: EN
technology:
  - Material
  - Angular
date: 2021-10-01T09:13:08.110Z
---
When using Material Tooltip, you need to pass a string for the Angular Material component to work. However, I came across multiple requests to have the displayed message on separate lines to make it easier to view for the users. Good thing is that Angular Material Tooltip accepts template literals whichy fully takes multi line into account so this is how I usually do it:

component.html

```html
<div 
     [matTooltip]="getFormattedText(object.propertyForTooltip)"
     matTooltipClass="tooltip-class"
>{{object.text}}</div>
```

component.ts

```javascript
public getFormattedText(tooltipText: string): string {
  if (tooltipText !== '') {
    let lines: string[];
    //it was our convention with the back end that '<br/>' would mark the new line
    lines = tooltipText.split('<br/>');
    let formattedText: string = ``;
    lines.forEach(line => {
      formattedText += `
        ${line}`;
    });
    return formattedText;
  }
  return tooltipText;
}
```

component.scss

```scss
.tooltip-class {
  white-space: pre-line;
  max-width: unset;
}
```

Hope this helped!

Cheers,\
Lucian