---
layout: blog
title: git two remotes
category: Web Dev
language: EN
technology:
  - git
date: 2021-06-09T10:44:26.419Z
---
Today the client decided it's time to migrate from another repo to a github repo and because the dev ops team is half way on the other side of the globe, they've asked me if I can do this code move. Although reluctant, I agreed to take on the challenge and here is how I saw it through:

1. I've added a new remote to the existing repo with:

git remote add github http://github.com/my-repo.git

This creates a relationship between the link and github as name for the new remote.

2. I've branched existing dev into a new branch from where I decided to do the pull request towards new dev

git checkout -b github-dev

3. I've pushed old dev to a new branch on the new repo from github

git push github github-dev

4. I've set the new dev to follow the new dev branch on github with:

git branch -u github/dev

Basically this tells the current branch (github-dev) to always have as upstream github/dev (remote/branch).

5. On git pull, the branch github-dev will bring updates from github's dev branch

And that's it really! Very complicated if you dont know where to start but very simple once you get the hang of it.

Hope this helped!

Cheers,\
Lucian