---
layout: blog
title: Angular Material Dialog Template styling
category: Web Dev
language: EN
technology:
  - Angular
  - MaterialDesign
  - TypeScript
date: 2021-12-16T16:02:53.496Z
---
I often use pop ups to offer a user experience guided towards making changes to specific elements. For example, a customer needs to see data in the form of a table and would like to edit specific records so I recommend adding an edit button to the record and triggering a pop up with the record specific info which can be edited. I also prefer creating new items in pop ups and most often these are fairly simple - they come with a title and a close button in the shame of an X, a form to display and edit and a lower area for the pop ups validation buttons such as "OK" and "Cancel".

A simple way to style pop ups and reuse the template is to add a class to styles.scss or, add a class to a scss file and have this imported in style.scss, as such:

dialog.scss

```scss
.dialog-create-edit {
  height: 635px;
  min-width: 1000px;
  
  .dialog-content {
    display: block;
    height: 580px;
    position: relative;
  }
  
  [mat-dialog-actions] {
    position: absolute;
    bottom: 0;
    right: 0;
    display: flex;
    justify-content: end;
  }
}
```

The styling above creates a fairly wide dialog and keeps the dialog action buttons in the lower right part of the screen

dialog-create.html

```html
<div class="dialog-content">
  <div clas="d-flex justify-content-between">
    <h2>Dialog Title</h2>
    <button mat-icon-button [mat-dialog-close]="true">
      <mat-icon>close</mat-icon>
    </button>
  </div>
  <form [formGroup]="dialogForm" class="d-flex my-2 flex-column">
    <mat-form-field [appearance]="'outline'">
      <mat-label>Field 1</mat-label>
      <input matInput [formControl]="formField1Control" placeholder="Insert..."/>
    </mat-form-field>
    <mat-form-field [appearance]="'outline'">
      <mat-label>Field 2</mat-label>
      <input matInput [formControl]="formField2Control" placeholder="Insert..."/>
    </mat-form-field>
  </form>
  <div mat-dialog-actions>
    <button mat-flat-button color="primary" [disabled]="condition()" (click)="onSubmit()" class="mr-2">Save</button>
    <button mat-flat-button color="accent" [mat-dialog-close]="true">Cancel</button>
  </div>
</div>
    
```

All that is left is passing the pannelClass when triggering the pop up and adding @import 'dialog.scss'; in styles.scss

dialog-trigger.ts

```typescript
[...]
 
 triggerModal() {
   const dialogRef = this.dialog.open(DialogCreateComponent, {
     panelClass: 'dialog-create-edit'
   });
   
   dialogRef.afterClosed().subscribe(response => console.log(response));
 }
```

what we do in dialog-trigger is we assign the scss class from dialog.scss to panelClass and this makes the styling available to any dialog which would come in this three area form (header, form, buttons) so I can re-use it easily through out my project.

Hope this helps!

Cheers,\
Lucian