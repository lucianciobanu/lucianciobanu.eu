---
layout: blog
title: Angular Reactive Form Implementation Checklist
category: Web Dev
language: EN
technology:
  - Angular
  - TypeScript
  - JavaScript
date: 2021-06-07T09:47:11.085Z
---
Note to self on quick form creation:

1. import { FormBuilder, FormGroup, Validators } from '@angular/forms';
2. private fb: FormBuilder -> add to constructor
3. in the constructor initiate this.form = this.fb.group({formControlName: \[null, Validators.required]) and add one property for each form control you need

   the formControlName key in the object can be any form control name you want as long as you use it between the " " on point 5 below.
4. in html do a form with directive \[formGroup]="form"
5. add formControlName="formControlName" to the select, radio or input you have inside your form and want to get data from.

with Angular its that easy!

Hope this helped!

Cheers,\
Lucian