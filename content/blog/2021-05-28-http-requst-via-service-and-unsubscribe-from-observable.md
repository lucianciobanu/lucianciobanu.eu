---
layout: blog
title: HTTP requst via service and unsubscribe from Observable
category: Web Dev
language: EN
technology:
  - Angular
  - JavaScript
date: 2021-05-28T12:07:34.865Z
---
As Angular makes a serious destinction between view, component and services, I find it important to clarify the process and have it written down.

In the component which needs the data, import the service and inject it in the constructor; I also add a subscription array so that I will unsubcribe from the observables OnDestroy and prevent data leakage.

```typescript
import { ComponentService } from '../component.service';
import { Subscription } from 'rxjs';

export class Component implements OnDestroy {
  priavate subscriptions: Subscription[] = [];
  constructor(private componentService: ComponentService) {}

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
  
  private getData() {
    const data = this.componentService.subscribe(
      receivedData => console.log(receivedData)
    );
    this.subscriptions.push(data);
  }
```

Cheers,\
Lucian