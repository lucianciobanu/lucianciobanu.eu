---
layout: blog
title: Datepicker Angular Material improved
category: Web Dev
language: EN
technology:
  - Angular
  - JavaScript
  - TypeScript
  - AngularMaterial
date: 2021-09-10T12:55:58.793Z
---
Following the implementation of Datepicker from Angular Material you will have an input field where the user can either input manually the date or click on the calendar to have a small dropdown with the dates of the respective month and the option to browse through months.

Now, for most of the projects I've implemented, we're mostly aiming to create an UX which only alows the user to click on the field to trigger the month dropdown and select the day from there and thus also creating a simple validation so that the user only inputs a date. 

I'm happy to put down a way to create such a date picker:

```javascript
<mat-form-field>
  <mat-label>Date</mat-label>
  <input matInput [formControl]="dateControl" [matDatepicker]="picker" (focus)="picker.open()" readonly>
  <mat-datepicker #picker></mat-datepicker>
</mat-form-field>
```

I think the awesome part is in triggering picker.open() on (focus) and the readonly attribute.

Hope this helped!

Cheers,\
Lucian