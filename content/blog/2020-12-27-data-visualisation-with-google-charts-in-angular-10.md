---
layout: blog
title: Data visualisation with Google Charts in Angular 10
category: Web Dev
language: EN
technology:
  - JavaScript
  - Angular2+
date: 2020-12-27T20:50:29.703Z
image: /images/uploads/screen-2020-12-27-la-23.21.38.png
---
During the second year of my Master's degree in Software Development and Business Information Systems, I'm attending the websystems development class which I was sure would challenge me in new ways. For my team's project, we are creating a web app to show the evolution of crypto currencies in the form of graphs. (project can be seen at [cryma.netlify.app](https://cryma.netlify.app))

After the painful realisation that Angular Material does not provide such a form of data visualisation, I looked towards other options and there are quite a few but balacing the pros and cons made me focus on Google Charts for Angular.

In order to include this in my Angular 10 project, I did

`npm install angular-google-charts`

in the comand line.

Then, I imported the Google Charts Module in the project's **app.module.ts** file as such:

```typescript
import { GoogleChartsModule } from 'angular-google-charts';

@NgModule({
  declarations: [
    AppComponent,
    CoinDisplayComponent
  ],
  imports: [
    BrowserModule,
    GoogleChartsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTabsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Then, in the component I want to display the chart (CoinDisplayComponent) I used the following HTML:

```html
<google-chart #chart
   [title]="title"
   [type]="type"
   [data]="data"
   [columns]="columnNames"
   [options]="options"
   [width]="width"
   [height]="height">
</google-chart>
```

Something that bugged me for quite a bit was that previous to June of this year, google-chart was accepting a \[columnsNames] directive which was changed to \[columns] and it took me some time to figure this out starting from the following error: 

`ERROR in Can't bind to 'columnNames' since it isn't a known property of 'google-chart'.`

`1. If 'google-chart' is an Angular component and it has 'columnNames' input, then verify that it is part of this module.`

And in the .ts file of this component, I've added the following code:

```typescript
title: string;
type = 'LineChart';
data = [];
columnNames = ["Day"];
options = {   
  hAxis: {
     title: 'Date'
  },
  vAxis:{
     title: 'USD'
  },
};
width = 550;
height = 400;
```

For me, "title", "data" and "columnNames" properties have assigned data coming from an HTTP request to coinapi.io but, If I were to type in the data my self, it would look like this:

```javascript
title = 'BTC';
type = 'LineChart';
data = [
  ["25/12/2020", 23000.40],
  ["26/12/2020", 23010.60],
  ["27/12/2020", 23020.80]
];
columnNames = ["Day", "BTC"];
options = {   
  hAxis: {
     title: 'Date'
  },
  vAxis:{
     title: 'USD'
  },
};
width = 550;
height = 400;
```

Which would show the following graph in the app:

![](/images/uploads/screen-2020-12-27-la-23.21.38.png "Google Chart in Angular App")

Hope this helped!

Cheers,\
Lucian