---
layout: blog
title: Celebrating Comments
category: Web Dev
language: EN
technology:
  - React
  - JAMStack
  - CommentBox
date: 2022-04-04T11:20:37.974Z
---
Really happy to say I've added comments to my learning in public space and what a challenge this has been! When I created this space, I was just looking to have a place to throw my thoughts together so I picked the quickest satisfying solution I could ship. Little did I know I had entered the world of jamstack and static websites which I would have unlikely come across as a full time Angular Developer. So, since launch, this space is a testimony of most of my Angular challanges (most of my articles are on the topic of unit tests), but also a challenge in itself to develop a static website running on React with GraphQL.

Very happy to say, please leave a comment below!

Cheers,\
Lucian