---
layout: blog
title: Mat Table like table
category: Web Dev
language: EN
technology:
  - Angular
  - HTML
  - CSS
date: 2021-07-05T10:19:09.258Z
---
Small table I put together just like a material table. Why not just use material table, then? The customer really wanted to have 4 sticky rows on top, right under the header which requires some hacky material design. So this is how I implemented it from scracth:

```html
<div class="table">
  <div class="talbe-header">
    <div class="table-cell" *ngFor="let header of headerDesc" (click)="customSort(header.linkedColumn, header.sortStatus)">
      {{ header.header }}
      <mat-icon *ngIf="header.sortStatus === 'no'" class="show-hover">arrow_upward</mat-icon>
      <mat-icon *ngIf="header.sortStatus === 'asc'">arrow_upward</mat-icon>
      <mat-icon *ngIf="header.sortStatus === 'desc'">arrow_downward</mat-icon>   
    </div>
  </div>
  <!-- display the fixed rows -->
  <div class="table-row" *ngFor="let record of totalRecords">
    <div class="table-cell">{{ record.firstCol }}</div>
    <div class="table-cell">{{ record.secondCol }}</div>
    <div class="table-cell">{{ record.thirdCol }}</div>
  </div>
  <!-- display rest of rows -->
  <div class="table-row" *ngFor="let record of dataSource.connect() | async">
    <div class="table-cell">{{ record.firstCol }}</div>
    <div class="table-cell">{{ record.secondCol }}</div>
    <div class="table-cell">{{ record.thirdCol }}</div>
  </div>
  <mat-paginator [pageSize]="10"></mat-paginator>
</div>
  
```

The styling looks as follows:

```scss
.table {
  display: flex;
  flex-flow: column norwap;
  font-size: 14px;
  line-height: 1.5;
  flex: 1 1 auto;
}

.table-header {
  display: flex;
  justify-content: center;
}

.table-header > .table-cell {
  justify-content: center;
  font-size: 12px;
  font-family: Roboto, "Helvetica-Neue", sans-serif;
  color: rgba(0, 0, 0, 0.54);
  font-weight: 500;
  
  .show-hover {
    display: none;
    color: #757575;
    
    &:hover {
      display: inline-block;
    }
  }
}

.table-row {
  width: 100%;
  display: flex;
  flex-flow: row nowrap;
}

.table-cell {
  display: flex;
  align-itmes: center;
  flex-grow: 1;
  flex-basis: 0;
  min-width: 0px;
  border-bottom: 1px solid #d0d0d0;
  min-height: 48px;
  text-align: center;
  justify-content: center;
}
    
```

I'm not going to go in the functionality too much but this allowed me to have a material table like looking custom table.

Hope this helped!

Cheers,\
Lucian